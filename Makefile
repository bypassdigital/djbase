all: start-django

start-django:
	@poetry run ./manage.py runserver

migrate:
	@poetry run ./manage.py migrate

static:
	@poetry run ./manage.py collectstatic

sh:
	@poetry run ./manage.py shell

dev-deps:
	@poetry install

prod-deps:
	@poetry install --no-dev -Ewsgi

.POSIX:
.SUFFIXES:
.PHONY: all start-django migrate static sh dev-deps prod-deps
