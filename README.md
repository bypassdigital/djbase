# Django Base
A project template to bootstrap common projects.

## Getting Started
Chose one of the stage you want to bootstrap and run the commands.

**development**
```
make dev-deps
cp .ex/dev.py base/settings/
# edit base/settings/dev.py
make migrate
make
```

**production**
```
make prod-deps
cp .ex/prod.py base/settings/
# edit base/settings/prod.py
make migrate
make static
# set up a wsgi daemon and a fcgi daemon
```


## Project Structure
Important project files:

<dl>
<dt>/base/</dt>
<dd>Contains the entrypoint of the project.  It has the settings, route definitions, and list of enabled apps.</dd>

<dt>/page/</dt>
<dd>This is a demo app that contains possible common pages across projects.</dd>

<dt>/templ/</dt>
<dd>Contains all the template files.</dd>

<dt>/static/</dt>
<dd>Contains all the static files.</dd>

<dt>/.ex/</dt>
<dd>Contains all the example files needed to bootstrap development or production stage.</dt>

<dt>/pyproject.toml, /poetry.lock
<dd>Dependency management files.</dd>

<dt>/manage.py</dt>
<dd>Contains the Django-specific commands.</dd>

<dt>/.editorconfig</dt>
<dd>You know what to do!</dd>

<dt>/Makefile</dt>
<dd>It has common shortcuts to manage the project.  Checkout the «Make Targets» section.</dd>
</dl>


## Make Targets
Here is a list of make(1) targets:

<dl>
<dt>all, or the default target</dt>
<dd>It just runs the project.</dd>

<dt>dev-deps</dt>
<dd>Installs all the development dependencies.</dd>

<dt>prod-deps</dt>
<dd>Installs all the production dependencies.</dd>

<dt>sh</dt>
<dd>A shortcut for the «shell» command in manage.py.</dd>

<dt>migrate</dt>
<dd>Runs the «migrate» command in manage.py.</dd>

<dt>static</dt>
<dd>Runs the «collectstatic» command in manage.py.</dd>
</dl>
