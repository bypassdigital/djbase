from django.shortcuts import render


def home(r):
	return render(r, "page/home.html")

def about(r):
	return render(r, "page/about.html")

def contact(r):
	return render(r, "page/contact.html")
