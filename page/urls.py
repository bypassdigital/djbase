from django.urls import path

from . import views as v


app_name = "page"

urlpatterns = [
	path("ana-sayfa/",  v.home,    name="home"),
	path("hakkimizda/", v.about,   name="about"),
	path("iletisim/",   v.contact, name="contact"),
]
