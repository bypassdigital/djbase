from .common import BASE_DIR

DEBUG = True
PORT = "8000"
INTERNAL_IPS = ["localhost", "127.0.0.1", "::1"]
SECRET_KEY = "secret_me_harder"
ALLOWED_HOSTS = ["localhost", "127.0.0.1", "::1"]
DATABASES = {
	"default": {
		"ENGINE": "django.db.backends.sqlite3",
		"NAME": BASE_DIR / "db.sqlite3",
	},
}
